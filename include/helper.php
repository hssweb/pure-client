<?php
/** helper functions **/

function pure_client_call_api($path) {
  global $pure_client_api_url;
  $url = $pure_client_api_url.$path;
  debug($url, "Calling");
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  $result = curl_exec($curl);
  curl_close($curl);
  return json_decode($result, true);
}


function debug($diagnostic, $msg = "") {
  global $debug;
  if (! $debug) return;
  
  $backtrace = debug_backtrace();
  if (isset($backtrace[1])) {
    if ($backtrace[1]['function'] == "pure_client_call_api") {
      echo "METHOD: ".$backtrace[2]['class'].":".$backtrace[2]['function'];
      echo ", LINE: ".$backtrace[2]['line'];      
    } else {
      echo "METHOD: ".$backtrace[1]['class'].":".$backtrace[1]['function'];
      echo ", LINE: ".$backtrace[1]['line'];
    }
  }
  echo "<br>";
  if ($msg != "") {
    echo $msg.": ";
  }
  if (is_array($diagnostic)) {
      echo "<pre>";
      print_r($diagnostic);
      echo "</pre>";
  } else {
      echo $diagnostic;
      echo "<br>";
  }
  echo "<hr>";
}


