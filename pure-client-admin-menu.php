<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}

add_action( 'admin_menu', 'pure_client_menu' );
function pure_client_menu() {
	add_menu_page('Pure Client', 'Pure Client', 'manage_options', 'pure-client-menu', 'pure_client_connection_options', 'dashicons-id-alt');
	add_submenu_page( 'pure-client-menu', 'Pure Client - Connection details', 'Connection details', 'manage_options', 'pure-client-menu-connection', 'pure_client_connection_options' ); 		  
	add_submenu_page( 'pure-client-menu', 'Pure Client - Parameters', 'Parameters', 'manage_options', 'pure-client-menu-parameters', 'pure_client_parameters_options' ); 		
	add_submenu_page( 'pure-client-menu', 'Pure Client - Cron', 'Cron', 'manage_options', 'pure-client-menu-cron', 'pure_client_cron_options' ); 	
}

function pure_client_connection_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Connection details updated.</p></div>';
	}
	$pure_cconnection_options = array(
		'pure_client_proxy_url_settings' => ''		
		);

?>	
	<div class="wrap">
	<h2>Pure Client</h2>
<?php
	add_option('pure_client_connection_options', $pure_client_options);

	$options = get_option('pure_client_parameters_options');
?>	
	<form action="options.php" method="post">				
		<?php settings_fields('pure_client_connection_options'); ?>
		<?php do_settings_sections('pure_client_connection'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
	</form>	
	</div>
<?php		
}

function pure_client_parameters_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Parameters updated.</p></div>';
	}
	$pure_client_options = array(
		'pure_client_person_uuids_settings' => '',		
		'pure_client_org_uuids_settings' => ''
		);

?>	
	<div class="wrap">
	<h2>Pure Client</h2>
<?php
	add_option('pure_client_parameters_options', $pure_client_options);

	$options = get_option('pure_client_parameters_options');
?>	
	<form action="options.php" method="post">				
		<?php settings_fields('pure_client_parameters_options'); ?>
		<?php do_settings_sections('pure_client_parameters'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
	</form>	
	</div>
<?php		
}
 
$cron_key = uniqid();
function pure_client_cron_options() {
	global $cron_key;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Cron options updated.</p></div>';
	}
	$pure_client_options = array(	
		'pure_client_cron_key_settings' => $cron_key,
    'pure_client_cron_debug_settings' => $cron_debug
	);
	add_option('pure_client_cron_options', $pure_client_options);
?>	
	<div class="wrap">
	<h2>Pure Client</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_client_cron_options'); ?>
		<?php do_settings_sections('pure_client_cron'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_client_cron_options'); ?>
	</form>	
	</div>
<?php		
}


add_action('admin_init', 'pure_client_admin_init');
function pure_client_admin_init(){
	global $pure_client_db_version, $pure_client_current_db_version;
	/* if tables do not exist, then create them (this is in admin init so that it works on multisite) */
	if (! get_option('pure_client_db_version')) {
		require_once(PURECLIENT_PATH.'/install.php');	
		pure_client_install();
	}
	// initialise options array
  $connection_options = get_option('pure_client_connection_options');
	$parameters_options = get_option('pure_client_parameters_options');
	$cron_options 		= get_option('pure_client_cron_options');
	
	/* check to see if any db updates need to be made */
	$pure_client_current_db_version = get_option('pure_client_db_version');
	if (version_compare($pure_client_current_db_version, $pure_client_db_version, '<')) {
		require_once(PURECLIENT_PATH.'/update.php');
		pure_client_version_update();
	}

	register_setting( 'pure_client_connection_options', 'pure_client_connection_options', 'pure_client_connection_options_validate' );  
	register_setting( 'pure_client_parameters_options', 'pure_client_parameters_options', 'pure_client_parameters_options_validate' );
	register_setting( 'pure_client_cron_options', 'pure_client_cron_options', 'pure_client_cron_options_validate' );
	add_settings_section('pure_client_connection', '', 'pure_client_connection_section_text', 'pure_client_connection', 'pure_client_connection');
	add_settings_field('pure_client_proxy_url', 'Pure Proxy URL', 'pure_client_proxy_url_settings', 'pure_client_connection', 'pure_client_connection' );  
	add_settings_section('pure_client_parameters', '', 'pure_client_parameters_section_text', 'pure_client_parameters', 'pure_client_parameters');
	add_settings_field('pure_client_person_uuids', 'Pure Person UUIDs', 'pure_client_person_uuids_settings', 'pure_client_parameters', 'pure_client_parameters' );
	add_settings_field('pure_client_org_uuids', 'Pure Organisation UUIDs', 'pure_client_org_uuids_settings', 'pure_client_parameters', 'pure_client_parameters' );
	add_settings_section('pure_client_cron', '', 'pure_client_cron_section_text', 'pure_client_cron', 'pure_client_cron' );
	add_settings_field('pure_client_cron_key', 'Key for Cron', 'pure_client_cron_key_settings', 'pure_client_cron', 'pure_client_cron' );
  add_settings_field('pure_client_cron_debug', 'Show debug info', 'pure_client_cron_debug_settings', 'pure_client_cron', 'pure_client_cron' );
	
}

function pure_client_connection_section_text() {
	echo '<h3>Connection details</h3><p>Set the URL for your Pure Proxy installation.</p>';
}

function pure_client_parameters_section_text() {
	echo '<h3>Parameters</h3><p>Specify the data you wish to retrieve from Pure. Specifying 
	an organisation\'s Pure UUID will retrieve all of the people associated with that organisation.</p>
		<p>UUIDs should be in a comma separated list. Line breaks are allowed and ignored.</p>
    <p>An example of a UUID is f81d4fae-7dec-11d0-a765-00a0c91e6bf6.</p>';
}

function pure_client_cron_section_text() {
	global $cron_key;
	$options = get_option( 'pure_client_cron_options' );
	$cron_key = ($options['pure_client_cron_key_settings'] != "") ? $options['pure_client_cron_key_settings'] : $cron_key;	
	$cron_link = plugins_url( 'cron.php?key='.$cron_key , __FILE__ ) ;
	
	echo '<h3>Cron</h3><p>You should set up a cron to update the local cache. For example, to update every day at 2am:</p>
		<code>0 2 * * * curl '.$cron_link.' >/dev/null 2>&1</code>';
}

function pure_client_proxy_url_settings() {
	$options = get_option('pure_client_connection_options');
	$value = htmlentities ( $options['pure_client_proxy_url_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
  echo "<input id='pure_client_proxy_url_settings' name='pure_client_connection_options[pure_client_proxy_url_settings]' size='50' type='text' value='{$value}' placeholder='http://www.pure-proxy.example.ed.ac.uk/wp-json/pure-api/v1' />";
}

function pure_client_cron_debug_settings() {
	global $cron_debug;
	$options = get_option('pure_client_cron_options');
	$value = htmlentities ( $options['pure_client_cron_debug_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = $cron_debug;
	}
	echo "<input id='pure_client_cron_debug_settings' name='pure_client_cron_options[pure_client_cron_debug_settings]' size='50' type='checkbox' value='1' " . checked(1, $options['pure_client_cron_debug_settings'], false) . " />";
  echo "<label for='pure_client_cron_debug_settings'></label>";
}

function pure_client_person_uuids_settings() {
	$options = get_option('pure_client_parameters_options');
	$value = htmlentities ( $options['pure_client_person_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_client_person_uuids_settings' name='pure_client_parameters_options[pure_client_person_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_client_org_uuids_settings() {
	$options = get_option('pure_client_parameters_options');
	$value = htmlentities ( $options['pure_client_org_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_client_org_uuids_settings' name='pure_client_parameters_options[pure_client_org_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_client_cron_key_settings() {
	global $cron_key;
	$options = get_option('pure_client_cron_options');
	$value = htmlentities ( $options['pure_client_cron_key_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = $cron_key;
	}
	echo "<input id='pure_client_cron_key_settings' name='pure_client_cron_options[pure_client_cron_key_settings]' size='50' type='text' value='{$value}' />";
}

function pure_client_connection_options_validate($input) {
	$options = get_option( 'pure_client_connection_options' );
	$options['pure_client_proxy_url_settings'] = trim($input['pure_client_proxy_url_settings']); 	
	return $options;
}
function pure_client_parameters_options_validate($input) {
	$options = get_option( 'pure_client_parameters_options' );
	$options['pure_client_person_uuids_settings'] = trim($input['pure_client_person_uuids_settings']); 	
	$options['pure_client_org_uuids_settings'] = trim($input['pure_client_org_uuids_settings']); 			
	return $options;
}
function pure_client_cron_options_validate($input) {
	$options = get_option( 'pure_client_cron_options' );
	$options['pure_client_cron_key_settings'] = trim($input['pure_client_cron_key_settings']);	
	$options['pure_client_cron_debug_settings'] = intval($input['pure_client_cron_debug_settings']);  
	return $options;
}
