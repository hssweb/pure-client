<?php
/*
 * This script should be called overnight from a cron. It could also be called by pressing a button in the control panel.
 */
if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( '../../../wp-load.php');
}
//define('PURECLIENT_PATH', plugin_dir_path(__FILE__) );
// require_once(PURECLIENT_PATH.'/models/Clipping.class.php');
require_once(PURECLIENT_PATH.'/models/Person.class.php');
// require_once(PURECLIENT_PATH.'/models/Project.class.php');
require_once(PURECLIENT_PATH.'/models/Publication.class.php');
require_once(PURECLIENT_PATH.'/models/Organisation.class.php');

// check that the key defined in options matches the one in the query string
$qs_key = $_REQUEST['key'];
$cron_options = get_option('pure_client_cron_options');
$options_key = $cron_options['pure_client_cron_key_settings'];
$debug = $cron_options['pure_client_cron_debug_settings'];

if ($qs_key == $options_key) {
 
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_person";
  $wpdb->query($truncate_sql);
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_person_information";
  $wpdb->query($truncate_sql);
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_organisation_association";
  $wpdb->query($truncate_sql);
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_staff_organisation_association";
  $wpdb->query($truncate_sql);			
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project";
  $wpdb->query($truncate_sql);
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_keywords";
  $wpdb->query($truncate_sql);			
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_person";
  $wpdb->query($truncate_sql);

  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_organisation";
  $wpdb->query($truncate_sql);			
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_publication_association";
  $wpdb->query($truncate_sql);
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication";
  $wpdb->query($truncate_sql);	

  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_keywords";
  $wpdb->query($truncate_sql);				

  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_person";
  $wpdb->query($truncate_sql);	
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_organisation";
  $wpdb->query($truncate_sql);			
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping";
  $wpdb->query($truncate_sql);				

  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping_reference";
  $wpdb->query($truncate_sql);				
  
  $truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping_person";
  $wpdb->query($truncate_sql);	  
  
	Person::get_pure_persons();
  
	$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_organisation";
	$wpdb->query($truncate_sql);
  
	$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_organisation_information";
	$wpdb->query($truncate_sql);   
      
	Organisation::get_pure_organisations();
  
	echo "Finished updating local cache";
} else {
	echo "Keys do not match";
}
