# README #

This plugin connects to any WordPress installation with the Pure Proxy plugin installed. It reads the JSON and caches all data in local tables. 

### Installation ###
1. Download the repository.
2. Unzip to extract the files and rename the folder so that you have a folder called pure-client. Zip this folder, naming it pure-client.zip.
3. Install using the Add Plugin option in your site’s dashboard. The plugin creates tables and options in the database so it’s important to install and uninstall using this method.
4. In your Dashboard, add the path to the Pure Proxy server, and the parameters (UUIDs of persons and/or organisations). 
5. Set up and run the cron to pull across the data.


### Pure Proxy plugin ###
The Pure Proxy plugin can be found [here](https://bitbucket.org/hssweb/pure-proxy). Note that the Pure Proxy plugin is only required if you do not already have access to a WordPress installation with it installed. It should be installed on a separate WordPress instance.

### Contribution ###
The plugin is maintained by the CAHSS Web Team at the University of Edinburgh. Contributions are welcome. Contact gavin.maxwell@ed.ac.uk.