<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}
	
class Person {

	public static function get_pure_persons() {
		$person_uuids = Person::get_pure_persons_uuids(); // get array of person UUIDs
		if (count($person_uuids) == 0 ) return;			
		Person::get_pure_persons_batch($person_uuids);
		Publication::get_publications("person", $person_uuids);
		//	Project::get_projects("person", $uuid);    
  }
	
	
	public static function get_pure_persons_uuids() {
		global $wpdb;
		$parameters_options = get_option('pure_client_parameters_options');
		$str_person_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_client_person_uuids_settings'])); // remove all whitespace	
		$org_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_client_org_uuids_settings'])); // remove all whitespace
    
		$person_uuids = explode(",", $str_person_uuids); // our main array

		/* get person UUIDs from organisation UUID */
		if ($org_uuids != "") {
      $organisation_associations = pure_client_call_api('/organisation_association?organisationuuid='.$org_uuids); 
      if (count($organisation_associations) > 0) {       
        foreach ($organisation_associations as $oa) {
          $person_uuids[]  = (string)$oa['personuuid'];			
        }        
      }
		}

		/* ensure person UUIDs are unique */
		if (count($person_uuids) > 1) {
			$p_uuids_unique = array_unique($person_uuids);
		} else {
			$p_uuids_unique = $person_uuids;
		}
		/* remove any empty entries */
		$p_uuids_unique = array_filter($p_uuids_unique);
		
		return array_values($p_uuids_unique); // ensuring array keys start at zero
	}
	
	public static function get_pure_persons_batch($uuids) {
		global $wpdb;
		if (count($uuids) == 0 ) return;
		$str_uuids = implode(",", $uuids);
    
    /* get all persons */
    $persons = pure_client_call_api('/person?personuuid='.$str_uuids);		// character limit of URL = 2048, so could cause problems with large numbers
    if ($persons['code'] == "rest_no_route") {
      echo $persons['message'];
      return;
    }    
		foreach ($persons as $p) {	
			/* insert person into table */
			$table_name = $wpdb->prefix . "pure_profiles_person";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'personuuid' => $p['personuuid'], 
				'employeeid' => $p['employeeid'], 				
				'portalurl' => $p['portalurl'],
				'firstname' => $p['firstname'], 
				'lastname' => $p['lastname'], 
				'callnamefirstname' => $p['callnamefirstname'],
				'callnamelastname' => $p['callnamelastname'],
				'title' => $p['title'], 
				'photos' => '',
				'retrieved' => $p['retrieved']
			));
      if (! $rows_affected) echo "Could not insert ".$p['personuuid']." into ".$table_name."<br>";
		}																		
			
    /* get all profiles */
    $person_information = pure_client_call_api('/person_information?personuuid='.$str_uuids);   
    foreach ($person_information as $pi) {  
      /* insert profile information into table */
      $table_name = $wpdb->prefix . "pure_profiles_person_information";
      $rows_affected = $wpdb->insert( $table_name, array( 
        'personuuid' => $pi['personuuid'], 
        'title' => $pi['title'], 
        'content' => $pi['content'], 
        'retrieved' => $pi['retrieved']
      ));			       
    }

    $organisation_associations = pure_client_call_api('/organisation_association?personuuid='.$str_uuids);
		foreach ($organisation_associations as $oa) {
      /* insert organisation associations into table */
      $table_name = $wpdb->prefix . "pure_profiles_organisation_association";
      $rows_affected = $wpdb->insert( $table_name, array( 
        'personuuid' => $oa['personuuid'], 
        'employeeid' => $oa['employeeid'], 
        'organisationuuid' => $oa['organisationuuid'],
        'organisationid' => $oa['organisationid'],
        'organisationname' => $oa['organisationname'],
        'organisationshortname' => $oa['organisationshortname'],
        'organisationtypeclassificationid' => $oa['organisationtypeclassificationid'],
        'organisationtypeclassification' => $oa['organisationtypeclassification'],
        'employmenttype' => $oa['employment_type'], 
        'employmenttypeid' => $oa['employmenttypeid'],
        'retrieved' => $oa['retrieved']
      ));
    }    
    

    $staff_organisation_associations = pure_client_call_api('/staff_organisation_association?personuuid='.$str_uuids);
		foreach ($staff_organisation_associations as $soa) {
      /* insert staff organisation associations into table */
			$table_name = $wpdb->prefix . "pure_profiles_staff_organisation_association";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'personuuid' => $soa['personuuid'],
				'employeeid' => $soa['employee_id'],
				'organisationuuid' => $soa['organisationuuid'], 
				'jobdescription' => $soa['jobdescription'],
				'retrieved' => $soa['retrieved']
			));			
    }
						
	}
		
}
		
