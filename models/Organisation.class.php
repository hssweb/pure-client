<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}

class Organisation {

	public static function get_pure_organisations() {
		global $wpdb;
		$parameters_options = get_option('pure_client_parameters_options');
		$str_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_client_org_uuids_settings'])); // remove all whitespace
    
    // get all organisations
    $organisations = pure_client_call_api('/organisation?organisationuuid='.$str_uuids);	
    if ($organisations['code'] == "rest_no_route") {
      echo $organisations['message'];
      return;
    }
    if ($organisations == NULL) return;
    
    foreach ($organisations as $o) {
			/* insert organisation into table */
			$table_name = $wpdb->prefix . "pure_profiles_organisation";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'organisationuuid' => $o['organisationuuid'], 
				'organisationid' => $o['organisationid'], 				
				'organisationname' => $o['organisationname'],
				'organisationshortname' => $o['organisationshortname'],
				'portalurl' => $o['portalurl'],
				'website' => $o['website'],
				'phone' => $o['phone'],
				'email' => $o['email'],
				'retrieved' => $o['retrieved']
			));
      if (! $rows_affected) echo "Could not insert ".$o['organisationuuid']." into ".$table_name."<br>";
    }

    /* get all organisation profiles */
    $organisation_profiles = pure_client_call_api('/organisation_information?organisationuuid='.$str_uuids);	
		foreach ($organisation_profiles as $op) {
      /* insert profile information into table */
      $table_name = $wpdb->prefix . "pure_profiles_organisation_information";
      $rows_affected = $wpdb->insert( $table_name, array( 
        'organisationuuid' => $op['organisationuuid'],
        'title' => $op['title'], 
        'content' => $op['content'], 
        'retrieved' => $op['retrieved']
      ));
    }

	//		Publication::get_publications("Organisation", $uuid);
	//		Project::get_projects("Organisation", $uuid);
	}
		
}



