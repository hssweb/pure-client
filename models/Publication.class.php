<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}
		
class Publication {

	public static function get_publications($type="person", $uuids) {
    global $wpdb;
    $publication_uuids = array();
   
    $uuids_str = implode(",",$uuids);
    $uuids_str = preg_replace('/\s+/', '', trim($uuids_str)); // remove all whitespace	
    
    $associated_publications = pure_client_call_api('/publication_'.$type.'?'.$type.'uuid='.$uuids_str);
    foreach ($associated_publications as $op) {
      $publication_uuids[] = $op['publicationuuid'];
    }
    $publication_uuids = array_unique($publication_uuids);
    debug($publication_uuids, "publication_uuids");
      
    foreach ($publication_uuids as $pub_uuid) {
        
      $publications = pure_client_call_api('/publication?publicationuuid='.$pub_uuid);
      foreach ($publications as $p) {
        
        /* insert publication into table */
        $table_name = $wpdb->prefix . "pure_profiles_publication";
        $rows_affected = $wpdb->insert( $table_name, array( 
          'publicationuuid' => $p['publicationuuid'], 			
          'title' => $p['title'], 
          'subtitle' => $p['subtitle'],
          'portalurl' => $p['portalurl'],
          'abstract' => $p['abstract'],
          'typeclassification' => $p['typeclassification'],
          'originallanguage' => $p['originallanguage'],
          'state' => $p['state'],
          'dois' => $p['dois'],
          'printisbns' => $p['printisbns'],
          'eisbns' => $p['eisbns'],
          'numberofpages' => $p['numberofpages'],
          'publicationyear' => $p['publicationyear'],
          'publicationmonth' => $p['publicationmonth'], 
          'publicationday' => $p['publicationday'], 
          'volume' => $p['volume'],
          'journalnumber' => $p['journalnumber'],
          'journalpages' => $p['journalpages'],
          'journaltitle' => $p['journaltitle'],
          'journalissn' => $p['journalissn'],
          'bibliographicalnote' => $p['bibliographicalnote'],
          'vancouver' => $p['vancouver'],
          'harvard' => $p['harvard'],
          'peerreview' => $p['peerreview'],
          'retrieved' => $p['retrieved']
        ));
        
        $publication_documents = pure_client_call_api('/publication_document?publicationuuid='.$p['publicationuuid']);
        foreach ($publication_documents as $pd) {
          $table_name = $wpdb->prefix . "pure_profiles_publication_document";			      
          $rows_affected = $wpdb->insert( $table_name, array( 
            'publicationuuid' => $pd['publicationuuid'], 			
            'url' => $pd['url'],
            'retrieved' => $pd['retrieved']
          )); 	        
        }
      
        $publication_keywords = pure_client_call_api('/publication_keywords?publicationuuid='.$p['publicationuuid']);
        foreach ($publication_keywords as $pk) {
          $table_name = $wpdb->prefix . "pure_profiles_publication_keywords";			
          $rows_affected = $wpdb->insert( $table_name, array( 
            'publicationuuid' => $pk['publicationuuid'],
            'keywordcontainerid' => $pk['keywordcontainerid'],
            'userdefinedkeywordid' => $pk['userdefinedkeywordid'],
            'keyword' => $pk['keyword'],
            'retrieved' => $pk['retrieved']
          )); 	        
        }
        
        
        $publication_persons = pure_client_call_api('/publication_person?publicationuuid='.$p['publicationuuid']);
        foreach ($publication_persons as $pp) {
          $table_name = $wpdb->prefix . "pure_profiles_publication_person";
          $rows_affected = $wpdb->insert( $table_name, array( 
            'publicationuuid' => $pp['publicationuuid'],
            'title' => $pp['title'], 
            'personuuid' => $pp['personuuid'],
            'firstname' => $pp['firstname'], 
            'lastname' => $pp['lastname'], 
            'personrole' => $pp['personrole'],
            'retrieved' => $pp['retrieved']
          ));
        }
        
        $publication_organisations = pure_client_call_api('/publication_organisation?publicationuuid='.$p['publicationuuid']);
        foreach ($publication_organisations as $po) {
          $table_name = $wpdb->prefix . "pure_profiles_publication_organisation";
          $rows_affected = $wpdb->insert( $table_name, array( 
            'publicationuuid' => $po['publicationuuid'], 
            'title' => $po['title'],
            'organisationuuid' => $po['organisationuuid'],
            'organisationid' => $po['organisationid'],
            'organisationname' => $po['organisationname'], 
            'organisationshortname' => $po['organisationshortname'],
            'orgtypeclassification' => $po['orgtypeclassification'], 
            'orgtypeclassificationid' => $po['orgtypeclassificationid'],
            'retrieved' => $po['retrieved']
          ));
        }
        
      }
      
    } // foreach ($publication_uuids as $pub_uuid) 
  } // function get_publications()
  
}	// class
