<?php
/*
Plugin Name: Pure Client
Description: This plugin connects to the HSS Pure API and processes the JSON.
Author: Gavin Maxwell, CAHSS Web Team, The University of Edinburgh
Version: 1.0
Author URI: http://www.ed.ac.uk/cahss/web-team
License: GPL3
*/

define('PURECLIENT_PATH', plugin_dir_path(__FILE__) );
$pure_client_db_version = "1.0";
$pure_client_current_db_version = "";
$connection_options = get_option('pure_client_connection_options');
$pure_client_api_url = $connection_options['pure_client_proxy_url_settings'];

require_once(PURECLIENT_PATH.'/pure-client-admin-menu.php');
require_once(PURECLIENT_PATH.'/models/Person.class.php');
require_once(PURECLIENT_PATH.'/models/Organisation.class.php');
require_once(PURECLIENT_PATH.'/models/Publication.class.php');

/* add settings link to plugins page */
function pure_client_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=pure-client-menu">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'pure_client_settings_link' );

